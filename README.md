# Aotearoa Co-incidence Network

Please reference:
Harvey, E.P., Hobbs, M., Looker,J., O’Neale, D.R.J., Scarrold, W and Turnbull, S.M (2021). Exploring COVID-19 Transmission Risk and Vulnerability Through the Aotearoa Co-incidence Network (ACN). Tech. Rep., Te Punaha Matatini. https://www.tepunahamatatini.ac.nz/2021/09/03/exploring-covid-19-transmission-risk-and-vulnerability-through-the-aotearoa-co-incidence-network/ 

## Primer
The following work uses the R programming language (R version 4.0.2), specifically designed for use with RStudio. These programs can be downloaded for free at https://www.rstudio.com/products/rstudio/. All analysis contained within this repository should be able to be run without prior knowledge of the R programming language. Firstly, download all files within this repository, either via git, or for those unfamilar with git, as a .zip file. The .zip file can be downloaded on the main repository page (https://gitlab.com/tpm-public-projects/aotearoa-connection-network) where it says "Download". 

In order to run scripts, certain packages are required. To install packages within R studio, click on `Tools` and then type in the name of the packages as listed below:
- tidyverse
- sf
- igraph
- leaflet
- leaflet.extras
- RColorBrewer
- rcartocolor
- viridis
- ggrepel
- cowplot
- fst
- shinyWidgets
- shinythemes 

Once files are accessable locally, open the file `ACN-Analysis.rmd` in RStudio, navigate to the top right of the toolbar within RStudio and click "Run", and then "Run All". This should produce various analysis, including visualisations and outputs. Following the running of `ACN-Analysis.rmd`, which generates files that are stored in `outputs`, the shiny app will also be able to be run. To run the app, navigate to the file `app.R`, open in RStudio, and click `Run` again. Alternatively, the app may be accessed at: stur600.shinyapps.io/aotearoa-coincidence-network/

Please contact Steven at s.turnbull@auckland.ac.nz if you have trouble accessing or running any files.

## 1) Introduction

The goal of this work is to create a network consisting of dwellings in each Statistical Area units (SA2) as nodes, with edges between these nodes representing shared contacts. Shared contacts are defined in terms of connections between a dwelling connecting to another dwelling via a shared workplace or educational institution (ECE, primary, intermediate, university etc.). This network can then be aggregated to SA2 level to meet confidentiality requirements and release from the IDI. 

### Files

**SQL:**

- `getDwellings_2018_census.sql` creates a list of individuals and their associated dwellings from census 2018.
- `getEducation_2018_moe.sql` creates a list of students who have been enrolled in some form of primary-intermediate-secondary education within the last 10 years. 
- `getWorkplaces_2018_IRcensusmonth.sql` creates a list of employees and their associated PBN (Permanent Business Number) - a unique identifier for a place of work.

**R Markdown:**

- `createDwellingNetwork_V4.rmd` runs `.sql` files and carries out the methodology.
- `DPMC_network_processing.Rmd` takes the raw output from the IDI and produces `WorkSchools_network_clean_shiny.rdata` which is used as input to the Shiny `app.r`.

**Inputs and Outputs:**

Make sure to have git-LFS intialised prior to working within the repo. Outputs will contain all files produced by `ACN-Analysis.rmd`, including all images used in the report. 

Inputs should contain all data that is needed for the app to work.

 Any inputs that are not necessary for the app but are used in the `ACN_analysis.rmd` will go in `supportingDocs` and won't be pushed to the repo (set big files to .gitignore) unless needed for some reason.

The folder `www` contains an image (.png) of affiliations that is used in the app. 



## 2) Methodology

The following section will outline the various steps required to create this network through use of data obtained through the Integrated Data Infrastructure (IDI). 

### 2.1) Dwelling Data

We begin by pulling the full list of individuals from Census 2018, with their recorded dwelling of usual residence, and the SA2 (and TA) where the dwelling is located. (see `getDwellings_2018_census.sql` for SQL query used). This table (Table 2.1.A) takes the following form:

**Table 2.1.A**  - Dwelling data frame

| snz_uid                    | ur_dwl_id   | ur_sa2        | ur_ta |
| -------------------------- | ----------- | ------------- | ----- |
| n~ 4,300,000 (primary key) | n~1,600,000 | 100100-400001 | 01-76 |

### 2.2) Workplace Data

We then pull a list of individuals present in Inland Revenue (IR) data for the month of census 2018 (March). This month is used to ensure that workplace information most accurately reflects the list of individuals in the 2018 census. More specifically, this data covers a selection of individuals who have a recorded relation to a Permanent Business Number (PBN) where the `return_period_date` in IR data was in March 2018, the record of income is for `W&S` (Work & Salary), and the PBN had an associated industry classification (ANZSIC06). This produces a table (Table 2.2.B) with the following form:

**Table 2.2.A**

| snz_uid                   | ir_ems_pbn_nbr | ir_ems_pbn_anzsic06_code | sa2_code      | ta_code |
| ------------------------- | -------------- | ------------------------ | ------------- | ------- |
| n~2,100,000 (primary key) | n~1,990,000    | A-S (1 level)            | 100100-400001 | 01-76   |

where `sa2_code` and `ta_code` are the SA2 and TA where the PBN is placed. Note - the population is not the  same as the population obtained in the census data. This is partly because many people (especially children) will not have a recorded place of work, and also many individuals with jobs may not necessarily have records present in IR, or vice versa in census.  



### 2.3) Education Data

We finally pull the list of individuals present in data provided by Ministry of Education (MoE). This involves students of ECE, primary-intermediate schools, and tertiary institutions. We draw on the pool of students who were enrolled in these education institutions in census year (2018). This forms a table (Table 2.3.A) as follows:

| snz_uid          | provider_code | type | sa2_code      | ta_code |
| ---------------- | ------------- | ---- | ------------- | ------- |
| n~ (primary key) |               |      | 100100-400001 | 01-76   |

where `type` refers to the type of school the individual attended (e.g., ECE, primary, university etc.) and `sa2_code ` and `ta_code` refer to the location of the educational institution.



## 3) Restructuring Input Data

To achieve the goal of creating a network of dwellings connected by shared connections between workplaces and schools, we then need to restructure our input data so that they are joined together. We do this by doing a `inner join` of the dwelling data to the workplace data, which means that a dataframe is produced which only includes individuals who appear in **both** the dwelling data and the workplace. This takes the form of Table 3.A below: 

**Table 3.A**

| snz_uid | ur_dwl_id | ur_sa2 | ur_ta | ir_ems_pbn_nbr | ir_ems_pbn_anzsic06_code | sa2_code | ta_code |
| ------- | --------- | ------ | ----- | -------------- | ------------------------ | -------- | ------- |
|         |           |        |       |                |                          |          |         |

Our goal is to produce an edge list, where we have `ur_dwl_id`s connected to other `ur_dwl_id`s, with a weight of how many shared workplaces are between them. We can do this by restructuring Table D as a *bipartite network* of dwellings connected to workplaces. To create this type of network we connect the two different node sets (dwellings vs workplaces) by the number of `snz_uid`s that are shared between them. This dataframe (Table 3.B) appears as follows:

| ur_dwl_id | ir_ems_pbn_nbr | snz_uid |
| --------- | -------------- | ------- |
| 1         | **555**        | 11      |
| 1         | 222            | 12      |
| 2         | **555**        | 93      |
| 3         | 111            | 89      |

In Table 3.B, we can see that `ur_dwl_id` (dwelling) "1" is connected to `ur_dwl_id` "2" through one connection at `ir_ems_pbn_nbr` (workplace) "555". With the data in this network format, we can then *project* onto one set of nodes in the bipartite network. In our case, we project onto the dwelling nodes to create a dataframe where `ur_dwl_id` connect to other `ur_dwl_id` , with a *weight* column that indicates how many shared connections these households have. For workplaces, we can break this weight down by the industry sectors of each PBN. This produces the following table (Table 3.C):

| From_dwl_id | To_dwl_id | Sector A | Sector B | Sector ... | Sector S |
| ----------- | --------- | -------- | -------- | ---------- | -------- |
| 1           | 2         | 1        | 0        | 0          | 0        |

Carrying forward the example of Table 3.B, dwelling 1 would be connected to dwelling 2, through 1 connection at PBN 555. If PBN 555 belongs to Sector A, this would produce a weight of 1. Other sectors have a weight of 0, given these two dwelling share no connections through PBNs in these areas. 

We can repeat this same process for the education data. To summarise:

1) Inner join dwelling data to education data (e.g., Table3.A)

2) Structure as bipartite network (e.g., Table 3.B)

3) Project onto dwelling nodes (e.g., Table 3.C). 

Once we have the dataframes for workplaces and education structured in terms of an edgelist of dwelling nodes and weighted connections, we can use a combine these into one table that takes the following appearance (Table 3.D):

**Table 3.D** 

| From_Dwl_ID | From_SA2 | To_Dwl_ID | To_SA2 | Sector_Anzsic06 (Split into sectors A-S) | Education (split into types) |
| ----------- | -------- | --------- | ------ | ---------------------------------------- | ---------------------------- |
|             |          |           |        | n                                        | n                            |
| …           |          | …         |        | …                                        | ...                          |

In Table 3.D, we have the dwelling pairs, with weights (*n*) indicating the number of shared connections for the two dwelling across Sectors - with multiple columns for Sectors A-S, and across Education, with multiple columns for different Education types. This table allows us to aggregate at whatever level may be useful - we may want to looks at the number of shared connections across specific sectors, or we may want to sum these columns to look at connections at workplaces overall, and the same for schools. Table 3.D also includes the SA2 information for each dwelling. This allows us to once more aggregate, this time to a high geographical resolution. Through aggregation we can make this data confidential, and produce a table (Table 3.E) as follows:

| From_SA2 | To_SA2 | Sector_Anzsic06 (Split into sectors A-S) | Education (split into types) |
| -------- | ------ | ---------------------------------------- | ---------------------------- |
|          |        | N                                        | N                            |
| …        | …      | …                                        | ...                          |

Where *N* is the sum of the individual counts for dwellings (*n*) for `From_Dwl_ID`  and `To_Dwl_ID` have been aggregated to the level of SA2. 

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg
