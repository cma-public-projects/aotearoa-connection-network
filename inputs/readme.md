The app requires the following files in this inputs folder

As at V1.2

shape files:

- SA2_sf.rdata
- TA_sf.rdata
- DHB_sf.rdata

network edgelist:

- symmetrical_network.fst

vulnerability data and network metrics:

- vulnerability_transmission_risk_combined.rdata