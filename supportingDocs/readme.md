The supportingDocs folder is ignored by .gitignore

Some of the larger files (IDI outputs) needed for running the different .Rmd files need to be placed (or symlinked) here. These files are:

- `Vulnerability_Data_April2021.csv` (which can be found in `covid-19-sharedFiles/data/spatialdata/vulnerability`)
- `WorkSchools_network_clean_shiny.RData` (which is produced by `DPMC_network_processing.Rmd` in the `complexContagion` git repo and output is saved in `covid-19-sharedFiles/data/IDI_processed_outputs/DPMCnetwork/inputs_v4`)

