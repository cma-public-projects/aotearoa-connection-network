---
title: "ACN-Schools-Analysis"
author: "Steven Turnbull"
date: "12/10/2021"
---

```{R}
library(tidyverse)
library(sf)
library(igraph)
library(leaflet)
library(RColorBrewer)
library(rcartocolor)
library(viridis)
library(ggrepel)
library(cowplot)
library(fst)
```

```{R}
#shape files
SA2_sf <- readRDS("inputs/SA2_sf.rdata")
DHB_sf <- readRDS("inputs/DHB_sf.rdata")
TA_sf <- readRDS("inputs/TA_sf.rdata")

#just labels 
SA2_labels <- SA2_sf %>% select(SA22018, SA22018_N, TA2018,TA2018_N) %>% mutate(SA22018 = as.numeric(SA22018))
st_geometry(SA2_labels) <- NULL
SA2_labels <- SA2_labels %>% 
  left_join(read.csv("supportingDocs/Vulnerability_Data_April2021.csv", fileEncoding = 'UTF-8-BOM') %>% select(SA2_code,IUR2018_name),
            by = c("SA22018" = "SA2_code"))

#network data
sym_network_df <- readRDS(file = "supportingDocs/WorkSchools_network_clean_shiny.rdata") %>%
    filter(from_SA2_node!='343000',to_SA2_node!='343000') %>%
    filter(All_Connections!=0)

network_df <- sym_network_df %>%
  #remove symmetrical edges
  mutate(from_SA2_node_new = pmin(from_SA2_node, to_SA2_node),
         to_SA2_node_new = pmax(from_SA2_node, to_SA2_node)) %>%
  select(-from_SA2_node,-to_SA2_node) %>%
  unique() %>%
  rename(from_SA2_node = "from_SA2_node_new",
         to_SA2_node = "to_SA2_node_new") %>%
  #rearrange cols to put to and from node first (needed for igraph)
    select(from_SA2_node,to_SA2_node,everything())  


```

# schools in auckland specific connections
```{R}
#make df of connections for schools
auckland_school_connection_plot_df <- SA2_sf %>%
  #join shape file to data
  inner_join(
               sym_network_df %>%
               filter(from_SA2_node == 134200) %>% #mount albert central
                 filter(All_Schools>1), by = c("SA22018" = "to_SA2_node"))

auckland_sf <- auckland_school_connection_plot_df %>%
    filter(TA2018_N == "Auckland") 


#plot connections
min_connections <- min(auckland_sf$All_Schools[auckland_sf$All_Schools>1])
max_connections <- max(auckland_sf$All_Schools)


all_schools_plot <- ggplot() +
  geom_sf(data = auckland_sf,
        aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==134200), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "bottom",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.65,174.95), ylim = c(-36.95,-36.8))+
  ggtitle("All Schools")

all_schools_legend <- get_legend(all_schools_plot)
all_schools_plot <- all_schools_plot + theme(legend.position = "none")

primary_schools_plot <- ggplot() +   
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Primary),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==134200), color = "black",alpha=0) +
  #labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.65,174.95), ylim = c(-36.95,-36.8))+
  ggtitle("Primary Schools")

intermediate_schools_plot <-ggplot() +
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Intermediate),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==134200), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.5,175), ylim = c(-36.65,-37))+
  ggtitle("Intermediate Schools")

secondary_schools_plot <- ggplot() +
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Secondary),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==134200), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.65,174.95), ylim = c(-36.95,-36.8))+
  ggtitle("Secondary Schools")

  ggdraw()+
  draw_plot(all_schools_plot,x = 0.25, y = 0.557, width = 0.4, height = 0.4) +
  draw_plot(primary_schools_plot,x = 0.05, y = 0.15, width = 0.4, height = 0.4)+
  draw_plot(secondary_schools_plot,x = 0.45, y = 0.15, width = 0.4, height = 0.4) +
  draw_plot(all_schools_legend,x = 0.25, y = -0.15, width = 0.4, height = 0.4) +
  draw_label("Mount Albert Central", x= 0.455,y=0.98)
  

ggsave("outputs/mount_albert_central_school_connections.pdf")
ggsave("outputs/mount_albert_central_school_connections.png")

```
```{R}
#make df of connections for schools
auckland_school_connection_plot_df <- SA2_sf %>%
  #join shape file to data
  inner_join(
               sym_network_df %>%
               filter(from_SA2_node == 154900) %>% #papatoetoe central
                 filter(All_Schools>1), by = c("SA22018" = "to_SA2_node"))

auckland_sf <- auckland_school_connection_plot_df %>%
    filter(TA2018_N == "Auckland") 


#plot connections
min_connections <- min(auckland_sf$All_Schools[auckland_sf$All_Schools>1])
max_connections <- max(auckland_sf$All_Schools)


all_schools_plot <- ggplot() +
  geom_sf(data = auckland_sf,
        aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==154900), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "bottom",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.7,175), ylim = c(-36.8725,-37.05))+
  ggtitle("All Schools")

all_schools_legend <- get_legend(all_schools_plot)
all_schools_plot <- all_schools_plot + theme(legend.position = "none")

primary_schools_plot <- ggplot() +   
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Primary),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==154900), color = "black",alpha=0) +
  #labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections),labels = scales::comma) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.7,175), ylim = c(-36.8725,-37.05))+
  ggtitle("Primary Schools")

intermediate_schools_plot <-ggplot() +
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Intermediate),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==154900), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections)) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.7,175), ylim = c(-36.8725,-37.05))+
  ggtitle("Intermediate Schools")

secondary_schools_plot <- ggplot() +
  geom_sf(data = auckland_sf,
          aes(geometry = geometry,
              fill = Schools_Secondary),
          color = adjustcolor("black",alpha.f = 0.1)) +
  geom_sf(data = auckland_sf %>% filter(SA22018==154900), color = "black",alpha=0) +
  labs(fill = str_wrap("Number of School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Sunset",na.value = "white", limits = c(min_connections,max_connections), labels = scales::comma) +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
    coord_sf(xlim = c(174.7,175), ylim = c(-36.8725,-37.05))+
  ggtitle("Secondary Schools")

   ggdraw()+
  draw_plot(all_schools_plot,x = 0.25, y = 0.557, width = 0.4, height = 0.4) +
  draw_plot(primary_schools_plot,x = 0.05, y = 0.15, width = 0.4, height = 0.4)+
  draw_plot(secondary_schools_plot,x = 0.45, y = 0.15, width = 0.4, height = 0.4) +
  draw_plot(all_schools_legend,x = 0.25, y = -0.15, width = 0.4, height = 0.4) +
  draw_label("Papatoetoe Central", x= 0.455,y=0.98)

ggsave("outputs/papatoetoe_school_connections.pdf")
ggsave("outputs/papatoetoe_school_connections.png")

```

# overall auckland
```{R}
  #make df of connections for schools
all_school_connection_plot_df <- SA2_sf %>%
#join shape file to data
  inner_join(
               sym_network_df %>% 
               #select specific areas
               group_by(to_SA2_node)%>% 
                summarise(All_Schools = sum(All_Schools)) %>%
               mutate(to_SA2_node = as.integer(to_SA2_node)),  by = c("SA22018"="to_SA2_node"))

auckland_sf <- all_school_connection_plot_df %>%
  left_join(read.csv("supportingDocs/Vulnerability_Data_April2021.csv", fileEncoding = 'UTF-8-BOM'), by = c("SA22018"="SA2_code")) %>%
  filter(DHB2015_name == "Auckland") %>% 
  filter(IUR2018_name !="Rural other") 
waitemata_auckland <- all_school_connection_plot_df %>%
  left_join(read.csv("supportingDocs/Vulnerability_Data_April2021.csv", fileEncoding = 'UTF-8-BOM'), by = c("SA22018"="SA2_code")) %>%
  filter(DHB2015_name == "Waitemata")
counties_auckland <- all_school_connection_plot_df %>%
  left_join(read.csv("supportingDocs/Vulnerability_Data_April2021.csv", fileEncoding = 'UTF-8-BOM'), by = c("SA22018"="SA2_code")) %>%
  filter(DHB2015_name == "Counties Manukau")
  


#get min/max PageRank for scale
min_school <- min(all_school_connection_plot_df$All_Schools)
max_school <- max(all_school_connection_plot_df$All_Schools)


auckland_plot <- ggplot() +
  geom_sf(data = auckland_sf %>% filter(All_Schools>1),
          aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.001)) +
  labs(fill = str_wrap("Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Emrld",na.value = "white") +
  theme(legend.position = "bottom",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
  ggtitle("Auckland")

school_legend <- get_legend(auckland_plot)
auckland_plot <- auckland_plot + theme(legend.position ="none")
  
#dhb insets
waitemata_auckland <- ggplot(data = waitemata_auckland) +
  geom_sf(aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle(paste0("Waitamat\u101")) + 
  scale_fill_carto_c(palette = "Emrld",na.value = "white",c(min_school,max_school))+
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(size = 10)
        ) 

counties_auckland <- ggplot(data = counties_auckland) +
  geom_sf(aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.001)) +
  theme_void() +
  ggtitle(paste0("Counties Manukau")) + 
  scale_fill_carto_c(palette = "Emrld",na.value = "white",c(min_school,max_school))+
  theme(legend.position = "none",
        panel.border = element_rect(colour = "black", fill=NA, size=0.8),
        plot.title = element_text(hjust = 1,size = 10)
        ) 


#combine
ggdraw() +
  draw_plot(waitemata_auckland, x = -0.065, y = 0.3, width = 0.6, height = 0.6) +
  draw_plot(auckland_plot, x = 0.27, y = 0.3, width = 0.5, height = 0.5) +
  draw_plot(counties_auckland, x = 0.5, y = 0.3, width = 0.6, height = 0.6) +
  #draw_label(x = 0.5,y=0.95,size =11.25,"All School Connections") +
  draw_plot(school_legend, x=0,y=-0.3)

ggsave("outputs/AotearoaCoincidenceNetwork_school_connections.pdf",width=10)
ggsave("outputs/AotearoaCoincidenceNetwork_school_connections.png",width=10)


```

```{R}
 Auckland_TA <- TA_sf %>% filter(TA2018_V1_ == "076")

auckland_sf <- all_school_connection_plot_df %>%
  filter(TA2018_N == "Auckland") 

auckland_TA_plot <- ggplot() +
  geom_sf(data = auckland_sf %>% filter(All_Schools>1),
          aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.001)) +
  labs(fill = str_wrap("Total Number of  School Connections",20)) +
  theme_void() +
  scale_fill_carto_c(palette = "Emrld",na.value = "white", label = scales::comma) +
  theme(legend.position = "bottom",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
  ggtitle("Auckland TA")
school_legend <- get_legend(auckland_TA_plot)
auckland_TA_plot <- auckland_TA_plot + theme(legend.position = "none")
#closer
auckland_central_plot <- ggplot() +
  geom_sf(data = auckland_sf %>% filter(All_Schools>1),
          aes(geometry = geometry,
              fill = All_Schools),
          color = adjustcolor("black",alpha.f = 0.001)) +
  labs(fill = str_wrap("Total Number of  School Connections",25)) +
  theme_void() +
  scale_fill_carto_c(palette = "Emrld",na.value = "white") +
  theme(legend.position = "none",
         legend.key.width = unit(1.5, 'cm'),
        panel.border = element_rect(colour = "black", fill=NA, size=0.5),
        plot.title = element_text(hjust = 0.5,size = 10)
        ) +
  coord_sf(xlim = c(174.5,175), ylim = c(-36.65,-37))+
  ggtitle("Central Auckland")

ggdraw() +
  draw_plot(auckland_TA_plot,x = -0.125, y = 0.15, width = 0.86, height = 0.86)+
  draw_plot(auckland_central_plot,x = 0.27, y = 0.15, width = 0.86, height = 0.86) +
  draw_plot(school_legend, x = 0.2665, y = -0.225, width = 0.6, height = 0.6)

ggsave("outputs/AotearoaCoincidenceNetwork_aucklandTA_school_connections.pdf",width=10)
ggsave("outputs/AotearoaCoincidenceNetwork_aucklandTA_school_connections.png",width=10)
```

# Community Detection

## all schools 
```{R}
#community detection for schools using infomap
g_school <- graph_from_data_frame(network_df %>%
                             select(matches(paste0("from_SA2_node|",
                                    "to_SA2_node|",
                                    "All_Schools"))) %>%
                             rename(weight = "All_Schools") %>%
                             filter(!is.na(weight)),
                           directed = F)

coms_g_school<- cluster_infomap(g_school)

V(g_school)$community<- membership(coms_g_school)

communities_school_df <- as_data_frame(g_school, what = "vertices")


SA2_community_school_sf <- SA2_sf %>%
  mutate(SA22018 = as.character(SA22018)) %>%
  mutate(X = st_coordinates(st_point_on_surface(.))[,1], #this mutate function gives the x y coords of the middle of the SA2
         Y = st_coordinates(st_point_on_surface(.))[,2])%>%  #these throw warnings but everything seems fine
  inner_join(communities_school_df, by = c("SA22018"="name"))

#colour palette
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
community_col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
pal_communities <- colorFactor(
            palette = community_col_vector,
            domain = SA2_community_school_sf$community)

#ggplot map image to save
auckland_coms <- SA2_community_school_sf %>%
  group_by(community) %>%
  filter(any(TA2018 == "076"))%>%
  summarise(geometry = st_union(geometry))

all_school_coms_plot <- ggplot(data = auckland_coms) +
  geom_sf(aes(geometry = geometry,
              fill = as.factor(community)),
          color = adjustcolor("black",alpha.f = 0.001)) +
  #add TA overlay
  geom_sf(data = Auckland_TA, aes(geometry = geometry), fill = NA,
          color = adjustcolor("black",alpha.f = 1)) +
  theme_void() +
  scale_fill_manual(values = rep(community_col_vector,2)) +
  theme(legend.position = "none") +
  coord_sf(xlim = c(174.25,175.5), ylim = c(-36.5,-37.25))+
  ggtitle(paste0("All Schools\nNumber of Communities: ", n_distinct(auckland_coms$community)))+
  theme(plot.title = element_text(size = 10,hjust = 0.5),
        panel.border = element_rect(colour = "black", fill=NA, size=0.8)
        )

ggsave("outputs/AotearoaCoincidenceNetwork_school_communities_auckland.pdf",all_school_coms_plot)
ggsave("outputs/AotearoaCoincidenceNetwork_school_communities_auckland.png",all_school_coms_plot)

```

## primary
```{R}
g_school <- graph_from_data_frame(network_df %>%
                             select(matches(paste0("from_SA2_node|",
                                    "to_SA2_node|",
                                    "Schools_Primary"))) %>%
                             rename(weight = "Schools_Primary") %>%
                             filter(!is.na(weight)),
                           directed = F)

coms_g_school<- cluster_infomap(g_school)

V(g_school)$community<- membership(coms_g_school)

communities_school_df <- as_data_frame(g_school, what = "vertices")
print(coms_g_school)

SA2_community_school_sf <- SA2_sf %>%
  mutate(SA22018 = as.character(SA22018)) %>%
  mutate(X = st_coordinates(st_point_on_surface(.))[,1], #this mutate function gives the x y coords of the middle of the SA2
         Y = st_coordinates(st_point_on_surface(.))[,2])%>%  #these throw warnings but everything seems fine
  inner_join(communities_school_df, by = c("SA22018"="name"))

#colour palette
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
community_col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
pal_communities <- colorFactor(
            palette = community_col_vector,
            domain = SA2_community_school_sf$community)


#ggplot map image to save
auckland_coms <- SA2_community_school_sf %>%
  group_by(community) %>%
  filter(any(TA2018 == "076"))%>%
  summarise(geometry = st_union(geometry))

primary_school_coms_plot <- ggplot(data = auckland_coms) +
  geom_sf(aes(geometry = geometry,
              fill = as.factor(community)),
          color = adjustcolor("black",alpha.f = 0.001)) +
  #add TA overlay
  geom_sf(data = TA_sf %>% filter(TA2018_V1_ == "076"), aes(geometry = geometry), fill = NA,
          color = adjustcolor("black",alpha.f = 1)) +
  theme_void() +
  scale_fill_manual(values = rep(community_col_vector,2)) +
  theme(legend.position = "none")+
  coord_sf(xlim = c(174.25,175.5), ylim = c(-36.5,-37.25))+
  ggtitle(paste0("Primary Schools\nNumber of Communities: ", n_distinct(auckland_coms$community))) +
  theme(plot.title = element_text(size = 10,hjust = 0.5),
        panel.border = element_rect(colour = "black", fill=NA, size=0.8))

ggsave("outputs/AotearoaCoincidenceNetwork_primary_school_communities_auckland.pdf",primary_school_coms_plot)
ggsave("outputs/AotearoaCoincidenceNetwork_primary_school_communities_auckland.png",primary_school_coms_plot)

```

## Intermediate
```{R}
g_school <- graph_from_data_frame(network_df %>%
                             select(matches(paste0("from_SA2_node|",
                                    "to_SA2_node|",
                                    "Schools_Intermediate"))) %>%
                             rename(weight = "Schools_Intermediate") %>%
                             filter(!is.na(weight)),
                           directed = F)

coms_g_school<- cluster_infomap(g_school)

V(g_school)$community<- membership(coms_g_school)

communities_school_df <- as_data_frame(g_school, what = "vertices")
print(coms_g_school)

SA2_community_school_sf <- SA2_sf %>%
  mutate(SA22018 = as.character(SA22018)) %>%
  mutate(X = st_coordinates(st_point_on_surface(.))[,1], #this mutate function gives the x y coords of the middle of the SA2
         Y = st_coordinates(st_point_on_surface(.))[,2])%>%  #these throw warnings but everything seems fine
  inner_join(communities_school_df, by = c("SA22018"="name"))

#colour palette
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
community_col_vector = rep(unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals))),5)
pal_communities <- colorFactor(
            palette = community_col_vector,
            domain = SA2_community_school_sf$community)


#ggplot map image to save
auckland_coms <- SA2_community_school_sf %>%
  group_by(community) %>%
  filter(any(TA2018 == "076"))%>%
  summarise(geometry = st_union(geometry))

intermediate_school_coms_plot <- ggplot(data = auckland_coms) +
  geom_sf(aes(geometry = geometry,
              fill = as.factor(community)),
          color = adjustcolor("black",alpha.f = 0.001)) +
  #add TA overlay
  geom_sf(data = TA_sf %>% filter(TA2018_V1_ == "076"), aes(geometry = geometry), fill = NA,
          color = adjustcolor("black",alpha.f = 1)) +
  theme_void() +
  scale_fill_manual(values = rep(community_col_vector,2)) +
  coord_sf(xlim = c(174.25,175.5), ylim = c(-36.5,-37.25))+
  theme(legend.position = "none")+
  ggtitle(paste0("Intermediate Schools\nNumber of Communities: ", n_distinct(auckland_coms$community)))+
  theme(plot.title = element_text(size = 10,hjust = 0.5),
        panel.border = element_rect(colour = "black", fill=NA, size=0.8))

ggsave("outputs/AotearoaCoincidenceNetwork_intermediate_school_communities_auckland.pdf",intermediate_school_coms_plot)
ggsave("outputs/AotearoaCoincidenceNetwork_intermediate_school_communities_auckland.png",intermediate_school_coms_plot)

```

## Secondary
```{R}
g_school <- graph_from_data_frame(network_df %>%
                             select(matches(paste0("from_SA2_node|",
                                    "to_SA2_node|",
                                    "Schools_Secondary"))) %>%
                             rename(weight = "Schools_Secondary") %>%
                             filter(!is.na(weight)),
                           directed = F)

coms_g_school<- cluster_infomap(g_school)

V(g_school)$community<- membership(coms_g_school)

communities_school_df <- as_data_frame(g_school, what = "vertices")
print(coms_g_school)

SA2_community_school_sf <- SA2_sf %>%
  mutate(SA22018 = as.character(SA22018)) %>%
  mutate(X = st_coordinates(st_point_on_surface(.))[,1], #this mutate function gives the x y coords of the middle of the SA2
         Y = st_coordinates(st_point_on_surface(.))[,2])%>%  #these throw warnings but everything seems fine
  inner_join(communities_school_df, by = c("SA22018"="name"))

#colour palette
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
community_col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
pal_communities <- colorFactor(
            palette = community_col_vector,
            domain = SA2_community_school_sf$community)


#ggplot map image to save
auckland_coms <- SA2_community_school_sf %>%
  group_by(community) %>%
  filter(any(TA2018 == "076"))%>%
  summarise(geometry = st_union(geometry))

secondary_school_coms_plot <- ggplot(data=auckland_coms) +
  geom_sf(aes(geometry = geometry,
              fill = as.factor(community)),
          color = adjustcolor("black",alpha.f = 0.001)) +
  #add TA overlay
  geom_sf(data = TA_sf %>% filter(TA2018_V1_ == "076"), aes(geometry = geometry), fill = NA,
          color = adjustcolor("black",alpha.f = 1)) +
  theme_void() +
  scale_fill_manual(values = rep(community_col_vector,2)) +
  theme(legend.position = "none")+
  coord_sf(xlim = c(174.25,175.5), ylim = c(-36.5,-37.25))+
  ggtitle(paste0("Secondary Schools\nNumber of Auckland TA Communities: ",n_distinct(auckland_coms$community))) +
  theme(plot.title = element_text(size = 10,hjust = 0.5),
        panel.border = element_rect(colour = "black", fill=NA, size=0.8))


ggsave("outputs/AotearoaCoincidenceNetwork_secondary_school_communities_auckland.pdf",secondary_school_coms_plot)
ggsave("outputs/AotearoaCoincidenceNetwork_secondary_school_communities_auckland.png",secondary_school_coms_plot)

```

```{R}
ggdraw()+
  draw_plot(all_school_coms_plot,x = 0.275, y = 0.5, width = 0.5, height = 0.5) +
  draw_plot(primary_school_coms_plot,x = 0.1, y = 0., width = 0.5, height = 0.5)+
  draw_plot(secondary_school_coms_plot,x = 0.45, y = 0., width = 0.5, height = 0.5)
ggsave("outputs/auckland_school_communities.pdf")
ggsave("outputs/auckland_school_communities.png")
```
