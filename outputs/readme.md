The outputs folder is ignored by .gitignore

Files produced (figures and output csvs) should get moved to Dropbox or similar - they will get overwritten if/when the `.Rmd` files are re-run.